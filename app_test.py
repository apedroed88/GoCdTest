import unittest
import json
from app import app

class AppTesteCase(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def teste_json_data(self):
        response = self.app.get('/')
        json_data = json.loads(response.data)
        self.assertDictEqual(json_data, dict(message='RUNNING')) 

if __name__ == '__main__':
    unittest.main()
